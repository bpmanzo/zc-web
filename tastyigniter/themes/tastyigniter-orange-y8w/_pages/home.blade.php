---
title: 'main::lang.home.title'
permalink: /
description: ''
layout: default
'[slider]':
    code: home-slider
'[featuredItems]':
    title: ''
    items: ['8', '9', '11']
    limit: !!float 3
    itemsPerRow: 3
    itemWidth: !!float 400
    itemHeight: !!float 300
'[localList]':
    distanceUnit: km
'[banners]':
    banner_id: 2
    width: !!float 940
    height: 360
---
@component('slider')

@component('banners')

@component('featuredItems')